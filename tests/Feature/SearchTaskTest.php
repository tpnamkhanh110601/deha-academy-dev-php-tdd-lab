<?php

namespace Tests\Feature;

use App\Models\Task;

;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_task_if_task_exists()
    {
        $this->login();
        $task = $this->createTask();
        $response = $this->get($this->getShowTaskRoute($task));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee(['name', 'content']);
    }

    /** @test */
    public function authenticated_user_can_not_get_task_if_task_not_exists()
    {
        $this->login();
        $response = $this->get($this->getShowTaskRoute(-1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertSee(['error']);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_task()
    {
        $task = $this->createTask();
        $response = $this->get($this->getShowTaskRoute($task));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function login()
    {
        return $this->actingAs(User::factory()->create());
    }

    public function getShowTaskRoute($id)
    {
        return route('tasks.show', $id);
    }

    public function createTask()
    {
        return $task = Task::factory()->create()->id;
    }
}
