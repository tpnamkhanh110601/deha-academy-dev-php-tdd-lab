<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_task_if_data_valid_and_task_exists()
    {
        $this->login();
        $task = $this->createTask();
        $data = Task::factory()->make()->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexTaskRoute());
        $this->assertDatabaseHas('tasks', $data);
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_task_not_exists()
    {
        $this->login();
        $data = Task::factory()->make()->toArray();
        $response = $this->put($this->getUpdateTaskRoute(-1), $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertSee(['error']);
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_name_field_is_null_and_task_exists()
    {
        $this->login();
        $task = $this->createTask();
        $data = Task::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task), $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_content_field_is_null_and_task_exists()
    {
        $this->login();
        $task = $this->createTask();
        $data = Task::factory()->make(['content' => null])->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task), $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function authenticated_user_can_not_update_task_if_name_and_content_field_is_null_and_task_exists()
    {
        $this->login();
        $task = $this->createTask();
        $data = Task::factory()->make(['content' => null, 'name' => null])->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task), $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function authenticated_user_can_see_update_form()
    {
        $this->login();
        $response = $this->get($this->getEditTaskRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function unauthenticated_user_can_not_update_task()
    {
        $response = $this->get($this->getEditTaskRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    public function login()
    {
        return $this->actingAs(User::factory()->create());
    }

    public function createTask()
    {
        return Task::factory()->create()->id;
    }

    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }

    public function getEditTaskRoute()
    {
        return route('tasks.edit');
    }

    public function getIndexTaskRoute()
    {
        return route('tasks.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
