<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_task_if_task_exisist()
    {
        $this->login();
        $task = $this->createTask();
        $taskBeforeDelete = Task::count();
        $response = $this->delete($this->getDeleteTaskRoute($task));
        $taskAfterDelete = Task::count();

        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals($taskBeforeDelete - 1, $taskAfterDelete);
    }

    /** @test  */
    public function authenticated_user_can_not_delete_task_if_task_not_exists()
    {
        $this->login();
        $response = $this->delete($this->getDeleteTaskRoute(-1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function unauthenticated_user_can_not_delete_task()
    {
        $task = $this->createTask();
        $response = $this->delete($this->getDeleteTaskRoute($task));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    public function getDeleteTaskRoute($id)
    {
        return route('tasks.destroy', $id);
    }

    public function login()
    {
        return $this->actingAs(User::factory()->create());
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function createTask()
    {
        return Task::factory()->create()->id;
    }
}
