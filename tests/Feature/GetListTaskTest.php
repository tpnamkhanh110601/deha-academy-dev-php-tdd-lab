<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
   /** @test */
    public function authenticated_user_can_get_tasks_list()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getIndexRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_tasks_list()
    {
        $response = $this->get($this->getIndexRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getLoginRoute());
    }

    public function getIndexRoute()
    {
        return route('tasks.index');
    }

    public function getLoginRoute()
    {
        return route('login');
    }
}
