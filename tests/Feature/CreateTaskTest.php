<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_task_if_data_vaild()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $taskBeforeCreate = Task::count();
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $taskAfterCreate = Task::count();

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $this->assertDatabaseHas('tasks', $task);
        $this->assertEquals($taskBeforeCreate + 1, $taskAfterCreate);
    }

    /** @test */
    public function authenticated_user_can_not_create_task_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function authenticated_user_can_not_create_task_if_content_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['content' => null])->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function authenticated_user_can_not_create_task_if_name_and_content_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null, 'content' => null])->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getLoginRoute());
    }

    public function getCreateTaskRoute()
    {
        return route('tasks.store');
    }

    public function getIndexRoute()
    {
        return route('tasks.index');
    }

    public function getLoginRoute()
    {
        return route('login');
    }
}
