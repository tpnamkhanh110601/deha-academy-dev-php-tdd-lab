<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('tasks/index', [TaskController::class, 'index'])
    ->name('tasks.index')
    ->middleware('auth');

Route::post('tasks/store', [TaskController::class, 'store'])
    ->name('tasks.store')
    ->middleware('auth');

Route::get('tasks/show/{id}', [TaskController::class,'show'])
    ->name('tasks.show')
    ->middleware('auth');

Route::put('tasks/update/{id}', [TaskController::class, 'update'])
    ->name('tasks.update')
    ->middleware('auth');

Route::get('tasks/edit', [TaskController::class, 'edit'])
    ->name('tasks.edit')
    ->middleware('auth');

Route::delete('tasks/destroy/{id}', [TaskController::class, 'destroy'])
    ->name('tasks.destroy')
    ->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
