<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function store(StoreTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return response()->json([$task], Response::HTTP_OK);
    }

    public function update(UpdateTaskRequest $request, $id)
    {
        $task = $this->task->findOrFail($id);
        $task->update($request->all());
        return redirect()->route('tasks.index');
    }

    public function edit()
    {
        return view('tasks.edit');
    }

    public function destroy($id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete();
        return response()->json([], Response::HTTP_OK);
    }
}
